import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import * as BooksAPI from './BooksAPI'
import Book from './components/book';

class SearchPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            books: [],
            results: []
        }
    }

    componentDidMount() {
        BooksAPI.getAll()
            .then(r => {
                this.setState({ books: r });
            })
    }


    updateBook = ((book, target) => {
        BooksAPI.update(book, target)
            .then((response) => {
                console.log(response);
                book.shelf = target;
                this.setState(state => ({
                    books: state.books.filter(b => b.id !== book.id).concat(book)
                }))
            })
    })

    activeSearch = (e) => {
        BooksAPI.search(e)
        .then(response => {
            this.setState({
                results: response
            })
        })
        .then(() => {
            console.log(this.state)
        })
    }   

    render(){
        return(
            <div className="search-books">
                <div className="search-books-bar">
                    <Link className="close-search" to="/">close</Link>
                    {/* <a className="close-search" onClick={() => this.setState({ showSearchPage: false })}>Close</a> */}
                    <div className="search-books-input-wrapper">
                        {/*
            NOTES: The search from BooksAPI is limited to a particular set of search terms.
            You can find these search terms here:
            https://github.com/udacity/reactnd-project-myreads-starter/blob/master/SEARCH_TERMS.md

            However, remember that the BooksAPI.search method DOES search by title or author. So, don't worry if
            you don't find a specific author or title. Every search is limited by search terms.
          */}
                        <input type="text" placeholder="Search by title or author" onChange={(e) => {this.activeSearch(e.target.value)}}/>
                    </div>
                </div>
                <div className="search-books-results">
                    <ol className="books-grid"></ol>
                        {this.state.results.map((r, k) => {
                           <Book k={k} b={r} updateBook={this.updateBook}/>
                        })}
                </div>
            </div>
        )
    }
}



export default SearchPage;