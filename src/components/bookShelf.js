import React, { Component } from 'react';
import Book from './book.js';

class BookShelf extends Component {


    render(){
        return(
            <div className="bookshelf">
                <h2 className="bookshelf-title">{this.props.name}</h2>
                <div className="bookshelf-books">
                    <ol className="books-grid">
                    {this.props.books.map((b, k) => <Book updateBook={this.props.updateBook} b={b} k={k} />)}
                        
                    </ol>
                </div>
            </div>
        )
    }
}

export default BookShelf;