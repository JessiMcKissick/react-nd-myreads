import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import BookShelf from './components/bookShelf.js';
import * as BooksAPI from './BooksAPI'



class MainPage extends Component{
    constructor(props) {
        super(props);
        this.state = {
            books:[]
        }
    }

    componentDidMount() {
        BooksAPI.getAll()
        .then(r => {
            this.setState({books: r});
        })
    }

    updateBook=((book, target) => {
        BooksAPI.update(book, target)
        .then((response) => {
            console.log(response);
            book.shelf = target;
            this.setState(state => ({
                books: state.books.filter(b => b.id !== book.id).concat(book)                
            }))
        })
    })

    render(){
        return (
            <div className="list-books">
                <div className="list-books-title">
                    <h1>MyReads</h1>
                </div>
                <div className="list-books-content">
                    <div>
                        {/* Put stuff here */}
                        <BookShelf updateBook={this.updateBook} name="Currently Reading" books={this.state.books.filter(b => b.shelf === "currentlyReading")} />
                        <BookShelf updateBook={this.updateBook} name="Want to read" books={this.state.books.filter(b => b.shelf === "wantToRead")} />
                        <BookShelf updateBook={this.updateBook} name="Read" books={this.state.books.filter(b => b.shelf === "read")} />
                    </div>
                </div>
                <div className="open-search">
                    <Link to="/search" >Add a book</Link>
                    {/* <a onClick={() => this.setState({ showSearchPage: true })}>Add a book</a> */}
                </div>
            </div>
        )
    }
}

export default MainPage;