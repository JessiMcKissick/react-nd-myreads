import React, {Component} from 'react'
import './App.css'
import {Route} from 'react-router-dom';
import MainPage from './mainPage.js';
import SearchPage from './search.js';
import * as BooksAPI from './BooksAPI'


class BooksApp extends Component {

  render() {
    return (
      <div>
        <Route exact path="/" render={() => (
          <MainPage />
        )}/>
        <Route exact path="/search" render={() => (
          <SearchPage />
        )}/>
      </div>
    )
  }
}

export default BooksApp